# PocketBook - Louis DUFOUR

## Informations projet
Application développer sous android API 28 pour ma part.

### Documentation
Un schéma d'architecture est encore en cour de construction.

![Image clique droit](/Documentation/AnalyseSonar.PNG)

*Je n'ai pas pu, vous trouvez dans la liste d'ajouts des permissions voici un screenshot qui résume la qualité de mon code*

### Cas d'erreur connus
- Getsion du tabs qui s'adapte 

#### Vu principale
- Button + *(TODO)*
- Button modifier *(TODO)*

#### Vu Tous
- Image par défaut *(TODO prévoire une image pardéfaut)*
- Button Tri *(TODO)*
- Button + *(TODO)*

#### Vu Detail
- Button:
	- Déplacer Livre *(TODO)*
	- Ajouter à la liste à lire plustard *(TODO)*
	- Changer le status *(Marche sur le detail, mais se met pas à jour dans la groupCollection)*
	- préter le livre *(TODO)*

#### Vu Emprunt/Emprunteur
- logic du switch button *(Début mis en place, mais pas compléter)*
- Button tri *(TODO)*
- Button + *(TODO)*

#### Vu Filtre
- Peut être mieux optimiser je pense
*ps: je sais pas pk sur mon tel lorsque l'appli est déployer la page ne veut même plus s'afficher. Par contre, lorsque je connecte mon téléphone avec un capable et je l'application via l'IDE cela marche parfaitement bien*

#### Vu Favoris
- pas faite si le passage du detail m'avais pas tant bloquer

### L'accès au code noté
- Projet ToolKitt
- Projet VMWrapper
- Projet BookApp Dossier ViewModel, UseCase, Composants, Pages

## Modèle

```mermaid
classDiagram
direction LR
class Book {
    +Id : string
    +Title : string
    +Publishers : List~string~
    +PublishDate : DateTime
    +ISBN13 : string
    +Series : List~string~
    +NbPages : int
    +Format : string
    +ImageSmall : string
    +ImageMedium : string
    +ImageLarge : string
}

class Languages {
    <<enum>>
    Unknown,
    French,
}

class Work {
    +Id : string
    +Description : string
    +Title : string
    +Subjects : List~string~
}

class Contributor {
    +Name : string
    +Role : string
}

class Author {
    +Id : string
    +Name : string
    +ImageSmall : string
    +ImageMedium : string
    +ImageLarge : string
    +Bio : string
    +AlternateNames : List~string~
    +BirthDate : DateTime?
    +DeathDate : DateTime?
}

class Link {
    +Title : string
    +Url : string
}

class Ratings {
    +Average : float
    +Count : int
}

Book --> "1" Languages  : Language
Book --> "*" Contributor : Contributors
Author --> "*" Link : Links
Work --> "*" Author : Authors
Work --> "1" Ratings : Ratings
Book --> "*" Author : Authors
Book --> "*" Work : Works

class Status {
    <<enum>>
    Unknown,
    Finished,
    Reading,
    NotRead,
    ToBeRead
}

class Contact{
    +string Id;
    +string FirstName;
    +string LastName;
}
```  

## Services & Interfaces
```mermaid
classDiagram
direction LR
Book --> "1" Languages  : Language
Book --> "*" Contributor : Contributors
Author --> "*" Link : Links
Work --> "1" Ratings : Ratings
Work --> "*" Author : Authors
Book --> "*" Work : Works
Book --> "*" Author : Authors

class IUserLibraryManager {
    <<interface>>
    +AddBook(Book book) : Task<Book>
    +AddBook(string id) : Task<Book>
    +AddBookByIsbn(string isbn) : Task<Book>
    +RemoveBook(Book book) : Task<bool>
    +RemoveBook(string id) : Task<bool>
    +RemoveBook(string id) : Task<bool>
    +AddToFavorites(Book book) : Task<bool>
    +AddToFavorites(string bookId) : Task<bool>
    +RemoveFromFavorites(Book book) : Task<bool>
    +RemoveFromFavorites(string bookId) : Task<bool>
    +UpdateBook(Book updatedBook) : Task<Book>
    +AddContact(Contact contact) : Task<Contact>
    +RemoveContact(Contact contact) : Task<bool>
    +LendBook(Book book, Contact contact, DateTime? loanDate) : Task<bool>
    +GetBackBook(Book book, DateTime? returnedDate) : Task<bool>
    +BorrowBook(Book book, Contact owner, DateTime? borrowedDate) : Task<bool>
    +GiveBackBook(Book book, DateTime? returnedDate) : Task<bool>

    +GetCurrentLoans(int index, int count)
    +GetPastLoans(int index, int count)
    +GetCurrentBorrowings(int index, int count)
    +GetPastBorrowings(int index, int count)
    +GetContacts(int index, int count)
}

class ILibraryManager {
    <<interface>>
    +GetBookById(string id)
    +GetBookByIsbn(string isbn)
    +GetBooksByTitle(string title, int index, int count, string sort)
    +GetBooksByAuthorId(string authorId, int index, int count, string sort)
    +GetBooksByAuthor(string author, int index, int count, string sort)
    +GetAuthorById(string id)
    +GetAuthorsByName(string substring, int index, int count, string sort)
}

class Status {
    <<enum>>
}

IUserLibraryManager ..|> ILibraryManager
IUserLibraryManager ..> Status
IUserLibraryManager ..> Contact
IUserLibraryManager ..> Book
ILibraryManager ..> Book
ILibraryManager ..> Author
```

## Ressources 
- [Doc officielle MAUI](https://learn.microsoft.com/en-us/dotnet/maui/)
- [MAUI Community Toolkit](https://learn.microsoft.com/en-us/dotnet/communitytoolkit/maui/)
- [SF Symbols 5.0 (format SVG et PNG)](https://www.figma.com/file/7ATqS05m0VqE905x1NKS0D/SF-Symbols-5.0---5296-SVG-Icons-(Community)?type=design&node-id=5-1670&mode=design&t=75VRROmRWJsStAIv-0)
- [SF Symbols 5.0 (PNG only)](https://github.com/andrewtavis/sf-symbols-online/tree/master/glyphs)
  
## Voici les captures d'écran réalisées sur iPhone.  
  
### Page Bibliothèque
<img src="screens/Library.png" width="300"/>   

- Un clic sur "Tous" permet de naviguer vers la page affichant tous les livres de notre bibliothèque,
- Un clic sur "En prêt" permet de voir à qui on a prêté un livre et à qui on a emprunté un livre
- Un clic sur "À lire plus tard" affiche tous les livres de la bibliothèque marqués comme "à lire plus tard"
- Un clic sur Statut de lecture permet de regrouper les livres en trois catégories : "Lu", "À lire", "Non lu"
- Un clic sur "Favoris" affiche seulement les livres préférés de notre bibliothèque
- Finalement, ... vous pouvez oublier "Étiquettes"
- Un clic sur "Auteur" permet de filtrer les livres par auteur (cf. plus bas)
- Un clic sur "Date de publication" permet de filtrer les livres par année de publication (cf. plus bas)
- Un clic sur "Note" permet de trier les livres par notes (données par la communauté)
- Le bouton "Modifier" ne sera pas utilisé
- Le bouton "+" permet de rentrer de nouveaux livres (cf. plus bas).

### Affichage de la liste de livres
<img src="screens/AllBooks.png" width="300"/>  

- Le bouton "+" permet de scanner un nouveau livre (cf. plus bas).
- Le bouton avec les deux flèches permet de changer l'ordre de présentation des livres.
- Note : les livres sont regroupés par auteur

### Affichage d'un livre
<img src="screens/OneBook.png" width="300"/>
<img src="screens/OneBook2.png" width="300"/>

### Filtrage par auteur
Lorsqu'on clique sur un le filtre "Auteur" de la page "Mes Livres", on navigue vers la page suivante :  
<img src="screens/Auteurs.png" width="300"/>  

Un clic sur un de ces auteurs permet d'aller sur la même page que celle qui affiche la liste des livres, mais seuls les livres de cet auteur apparaitront.  

### Filtrage par date de publication
Lorsqu'on clique sur un le filtre "Date de publication" de la page "Mes Livres", on navigue vers la page suivante :  
<img src="screens/Dates.png" width="300"/>  

Un clic sur une de ces années permet d'aller sur la même page que celle qui affiche la liste des livres, mais seuls les livres publiée cette année apparaitront.  

### Emprunts / Prêts
Lorsqu'on clique sur le menu "En prêt" sur la page d'accueil, on peut voir la liste de nos emprunts et de nos prêts répartis par contacts. Par exemple :  
<img src="screens/emprunts.png" width="300"/>   
Le switch permet de voir les livres qu'on a prêtés regroupés par contacts, ou les livres qu'on a empruntés regroupés par prêteurs.  

### Le fameux bouton "+"
Un clic sur le bouton "+" fait apparaître un sous-menu permettant d'ajouter un livre à la bibliothèque :
- en scannant un code-barres (cf. plus bas)
- en scannant plusieurs code-barres
- en recherchant en ligne (par titre ou par auteur),
- en saisissant l'ISBN

<img src="screens/Popup.png" width="300"/>  

### Le scan
La page Scan pourra être réalisée plus tard.  

<img src="screens/Scan.jpeg" width="300"/>  

